package admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import be.ordina.msdashboard.EnableMicroservicesDashboardServer;
import de.codecentric.boot.admin.config.EnableAdminServer;

/**
 * Hello world!
 *
 */
@EnableAdminServer
@EnableDiscoveryClient
@EnableMicroservicesDashboardServer
@SpringBootApplication
public class SpringAdminServer {
	
	public static void main(String[] args) {
		SpringApplication.run(SpringAdminServer.class, args);
	}
	
}
